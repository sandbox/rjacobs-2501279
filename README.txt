General Information
-------------------

This simple utility module offers the ability for managed files (file field
files) to be replaced without changing the path to the file and without the need
for additional admin and editorial interfaces. It adds a "Replace" option on
each file field widget that triggers a modal file replace upload form. The
replacement file assumes the exact same path, file name and file ID as the
original. Any validation rules enforced by the file field are also re-enforced
for the replacement.



Installation and Usage
----------------------

Please see the module documentation for full installation and usage details:
https://www.drupal.org/sandbox/rjacobs/2501279



Drupal Module Author
--------------------

Ryan Jacobs (rjacobs)
http://drupal.org/user/422459
http://www.ryan-jacobs.net